package co.simplon.alt6.jfxfiles;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

/**
 * Small wrapper class for File with two implementations, FileEntry and FolderEntry.
 * It represents a directory entry whether it is a directory or a file.
 */
public abstract class Entry {
    /**
     * Wrapped File
     */
    public File file;
    /**
     * Cached BasicFileAttributes
     */
    private BasicFileAttributes attributes = null;

    public Entry(File file) {
        this.file = file;
    }

    /**
     * @return true if this entry is a directory, else false
     */
    abstract boolean isDirectory();

    /** Get the entry name.
     * @return entry name
     */
    public String getName() {
        return this.file.getName();
    }

    /**
     * @return true if the entry is hidden, else false
     */
    boolean isHidden() {
        return this.file.isHidden();
    }

    /**
     * Get the entry BasicFileAttributes.
     * @return basic file attributes
     */
    BasicFileAttributes getFileAttributes() {
        if (this.attributes == null) {
            try {
                this.attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return this.attributes;
    }

    /**
     * Get the entry size.
     * @return size
     */
    public long getSize() {
        return this.getFileAttributes().size();
    }

    /**
     * Get the entry creation time.
     * @return creation time
     */
    public FileTime creationTime() {
        return this.getFileAttributes().creationTime();
    }

    /**
     * Get the entry last access time.
     * @return last access time
     */
    public FileTime lastAccessTime() {
        return this.getFileAttributes().lastAccessTime();
    }

    /**
     * Get the entry last modified time.
     * @return last modified time
     */
    public FileTime lastModifiedTime() {
        return this.getFileAttributes().lastModifiedTime();
    }
}
