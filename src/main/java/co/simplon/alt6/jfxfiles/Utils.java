package co.simplon.alt6.jfxfiles;

public class Utils {
    /**
     * Copied from one of my other open source project
     * @author Arthur ROUSSEL/Cat Core
     */
    public enum Os {
        windows(new String[]{"windows", "Windows"}),
        macos(new String[]{"mac os x", "mac", "osx", "os x", "Mac OS X", "Mac", "OSX", "OS X", "darwin", "Darwin"}),
        linux(new String[]{"linux", "Linux"}),
        freebsd(new String[]{"freebsd", "FreeBSD"}),
        openbsd(new String[]{"openbsd", "OpenBSD"}),
        unknown(new String[]{});

        public static final Os CURRENT_OS = getOs();

        final String[] aliases;

        Os(String[] aliases) {
            this.aliases = aliases;
        }
        Os(String alias) {
            this(new String[]{alias});
        }

        public boolean isUnknown() {
            return this == unknown;
        }

        public boolean isWindows() {
            return this == windows;
        }

        public boolean isMacOs() {
            return this == macos;
        }

        public boolean isLinux() {
            return this == linux;
        }

        public boolean isFreeBSD() {
            return this == freebsd;
        }

        public boolean isOpenBSD() {
            return this == openbsd;
        }

        public boolean isUnixLike() {
            return isMacOs() || useLinuxImplementation();
        }

        public boolean useLinuxImplementation() {
            return isLinux() || isBSDLike();
        }

        public boolean isBSDLike() {
            return isFreeBSD() || isOpenBSD();
        }

        private boolean match() {
            String name = System.getProperty("os.name");

            for (String alias : aliases) {
                if (name.contains(alias)) return true;
            }

            return false;
        }

        private static Os getOs() {
            for (Os os: values()) {
                if (os.match()) return os;
            }

            return Os.unknown;
        }
    }
}
