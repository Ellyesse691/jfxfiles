package co.simplon.alt6.jfxfiles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class FileEntry extends Entry {
    public FileEntry(File file) {
        super(file);
    }

    /**
     * Get the file content
     * @return file content as String
     */
    String readFileContent() {
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(this.file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }

    @Override
    boolean isDirectory() {
        return false;
    }

    /**
     * Get the file entry extension.
     * @return file extension
     */
    String getExtension() {
        String name = this.getName();

        String[] parts = name.split("\\.");

        return parts.length > 1 ? parts[parts.length - 1] : name;
    }
}
