package co.simplon.alt6.jfxfiles;

import java.util.Comparator;
import java.util.List;

public enum FileOrdering {
    NAME {
        @Override
        public Comparator<Entry> get() {
            return Comparator.comparing(Entry::getName);
        }
    }, ENTRY_TYPE {
        @Override
        public Comparator<Entry> get() {
            return Comparator.comparing(Entry::isDirectory);
        }
    }, SIZE {
        @Override
        public Comparator<Entry> get() {
            return Comparator.comparing(Entry::getSize);
        }
    }, CREATION {
        @Override
        public Comparator<Entry> get() {
            return Comparator.comparing(Entry::creationTime);
        }
    }, ACCESS {
        @Override
        public Comparator<Entry> get() {
            return Comparator.comparing(Entry::lastAccessTime);
        }
    }, MODIFICATION {
        @Override
        public Comparator<Entry> get() {
            return Comparator.comparing(Entry::lastModifiedTime);
        }
    };

    public abstract Comparator<Entry> get();

    public static void order(List<Entry> list, FileOrderingEntry... orderings) {
        Comparator<Entry> comparator = null;

        if (orderings.length > 0) {
            comparator = orderings[0].ordering.get();

            if (orderings[0].reversed) {
                comparator = comparator.reversed();
            }

            if (orderings.length > 1) {
                for (int i = 1; i < orderings.length; i++) {
                    Comparator<Entry> comp = orderings[i].ordering.get();

                    if (orderings[i].reversed) {
                        comp = comp.reversed();
                    }

                    comparator = comparator.thenComparing(comp);
                }
            }
        }

        if (comparator != null) {
            list.sort(comparator);
        }
    }

    public record FileOrderingEntry(FileOrdering ordering, boolean reversed) {
        public FileOrderingEntry(FileOrdering ordering) {
            this(ordering, false);
        }
    }
}
