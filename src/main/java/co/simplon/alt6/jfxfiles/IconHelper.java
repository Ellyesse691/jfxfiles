package co.simplon.alt6.jfxfiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public enum IconHelper {
    PICTURE("png", "jpg", "gif", "bmp", "jpeg") {
        @Override
        public InputStream getIcon(File file) throws FileNotFoundException {
            return new FileInputStream(file);
        }

        @Override
        String getIconPath(File file) {
            return file.toString();
        }
    }, TEXT("text", "txt") {
        @Override
        public InputStream getIcon(File file) {
            return IconHelper.class.getResourceAsStream(getTextFileIconPath());
        }

        @Override
        String getIconPath(File file) {
            return getTextFileIconPath();
        }
    }, DEFAULT() {
        @Override
        public InputStream getIcon(File file) {
            return IconHelper.class.getResourceAsStream(getBinFileIconPath());
        }

        @Override
        String getIconPath(File file) {
            return getBinFileIconPath();
        }
    };

    private final String[] extensions;

    IconHelper(String... extensions) {
        this.extensions = extensions;
    }

    private boolean match(String extension) {
        if (extensions.length == 0) return true;

        for (String ext : extensions) {
            if (ext.equals(extension)) return true;
        }

        return false;
    }

    public static String getIconPath(Entry entry) {
        String string = null;

        if (entry instanceof FileEntry fileEntry) {
            for (IconHelper helper : values()) {
                if (helper.match(fileEntry.getExtension())) {
                    string = helper.getIconPath(fileEntry.file);
                    break;
                }
            }
        } else if (entry instanceof FolderEntry) {
            string = getFolderIconPath();
        }

        return string;
    }

    public static String getTextFileIconPath() {
        return "/file.png";
    }

    public static String getBinFileIconPath() {
        return "/file_binary.jpg";
    }

    public static String getFolderIconPath() {
        return "/folder.png";
    }

    public static InputStream getEntryIcon(Entry entry) throws FileNotFoundException {
        InputStream inputStream = null;

        if (entry instanceof FileEntry fileEntry) {
            for (IconHelper helper : values()) {
                if (helper.match(fileEntry.getExtension())) {
                    inputStream = helper.getIcon(fileEntry.file);
                    break;
                }
            }
        } else if (entry instanceof FolderEntry) {
            inputStream = getFolderIcon();
        }

        return inputStream;
    }

    private static InputStream getFolderIcon() {
        return IconHelper.class.getResourceAsStream(getFolderIconPath());
    }

    abstract InputStream getIcon(File file) throws FileNotFoundException;

    abstract String getIconPath(File file);
}
